""" Module for getting backup days """
import logging
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

LOG = logging.getLogger("DAY ")

class BackupDay(object):
    """ Class for getting backup days """
    # pylint: disable=too-many-instance-attributes

    # adds for directory marking
    IYEARLY = "y"
    IMONTHLY = "m"
    IWEEKLY = "w"

    def __init__(self, config, now=None):
        """ Constructor for BackupDay

        Args:
            now (datetime, optional): datetime representing the meant day. Defaults to now.
        """
        self.config = config

        if now is None:
            now = datetime.now()
        self.now = now

        self.year = now.year
        self.month = now.month
        self.day = now.day
        self.day_week = now.weekday()+1

        self.s_year = str(self.year)
        self.s_month = self._leading_zero_lt10(self.month)
        self.s_day = self._leading_zero_lt10(self.day)
        self.s_day_week = str(self.day_week)

    @classmethod
    def _leading_zero_lt10(cls, value):
        """ Adds leading zero to a value lower than 10

        Args:
            value (int): value to check

        Returns:
            string: 2-digit value
        """
        return "0"+str(value) if (0 <= value <= 9) else str(value)

    def _is_yearly(self):
        """ Checks if current date is yearly backup

        Returns:
             bool: True if self is yearly backup, False otherwise
        """
        # string for check if backup is yearly
        check_yearly = self.s_month + "-" + self.s_day
        # if yearly enabled and day is for yearly backup
        if self.config.hold_yearly > 0 \
          and check_yearly in self.config.days_for_yearly:
            return True
        return False

    def _is_monthly(self):
        """ Checks if current date is monthly backup

        Returns:
             bool: True if self is monthly backup, False otherwise
        """
        # if monthly is enabled
        if self.config.hold_monthly > 0:
            # and monthly backup is set to day of month
            if self.config.monthly_by_week_day is not True:
                # if day is for monthly backup
                if self.day is self.config.day_for_monthly:
                    return True
            # or monthly backup is set to first weekday in month
            elif self.config.monthly_by_week_day is True\
                and self.config.day_for_monthly is self.day_week:
                # if day is first possible weekday in month
                if self.month > (self.now - timedelta(days=7)).month:
                    return True
        return False

    def _is_weekly(self):
        """ Checks if current date is weekly backup

        Returns:
             bool: True if self is weekly backup, False otherwise
        """
        # if weekly is enabled and day is for weekly backup
        if self.config.hold_weekly > 0 \
          and self.config.day_for_weekly is self.day_week:
            return True
        return False

    def get_directory_string_now(self):
        """ Getting the directory of current date

        Returns:
             string: directory name of the current date on disk
        """
        if self._is_yearly():
            type_ = self.IYEARLY
        elif self._is_monthly():
            type_ = self.IMONTHLY
        elif self._is_weekly():
            type_ = self.IWEEKLY
        # day of week is excluded
        elif self.day_week in self.config.exclude:
            type_ = "none"
        # is daily backup
        else:
            type_ = "d"

        # concat string for directory path
        directory = self.config.directory_format.replace("TYPE", type_)
        directory = directory.replace("%Y", self.s_year)
        directory = directory.replace("%m", self.s_month)
        directory = directory.replace("%d", self.s_day)

        return directory

    def get_backup_list(self):
        """ Getting a list of backups to hold

        Returns:
            [string, ]: list of backup directories to hold
        """

        list_ = []
        if self.config.hold_yearly > 0:
            # for all years to hold backup
            for year_delta in range(self.config.hold_yearly, -1, -1):
                # get number of year
                year = self.year - year_delta
                # for each yearly backup event
                for s_day in self.config.days_for_yearly:
                    # get date string in format YYYY-MM-DD
                    s_day = str(year) + "-" + s_day
                    # parse date from string
                    day = datetime.strptime(s_day, "%Y-%m-%d")
                    # append to list for holding backups
                    list_.append(BackupDay(now=day, config=self.config))

        if self.config.hold_monthly > 0:
            # for all months to hold backup
            for month_delta in range(self.config.hold_monthly, -1, -1):
                # if monthly backup is set to day of month
                if self.config.monthly_by_week_day is not True:
                    # subtract month_delta from now
                    day = self.now - relativedelta(months=month_delta)
                # or monthly backup is set to first weekday in month
                else:
                    # get first of the actual month (to get 1st day of month)
                    day = self.now - relativedelta(days=self.now.day-1)
                    # subtract month_delta from now
                    day = day - relativedelta(months=month_delta)
                    # get the date for the specific day of week
                    day = self._next_weekday(day, self.config.day_for_monthly)

                # append to list for holding backups
                list_.append(BackupDay(now=day, config=self.config))

        if self.config.hold_weekly > 0:
            # for all weeks to hold backup
            for week_delta in range(self.config.hold_weekly, -1, -1):
                # get next day for weekly
                day = self._next_weekday(self.now, self.config.day_for_weekly)
                # get the relevant week
                day = day - timedelta(weeks=week_delta+1)
                # append to list for holding backups
                list_.append(BackupDay(now=day, config=self.config))

        if self.config.hold_daily > 0:
            # for all days to hold
            for day_delta in range(self.config.hold_daily, -1, -1):
                # get specific day
                day = self.now - timedelta(days=day_delta)
                # append to list for holding backups
                list_.append(BackupDay(now=day, config=self.config))

        return list_

    @classmethod
    def _next_weekday(cls, day, weekday):
        """ Getting the next day with specific day of week

        Args:
             day (datetime): the actual day
             weekday (int): the week of day wanted after/same day
                1 - Monday
                2 - Tuesday
                    ...
                7 - Sunday

        Returns:
            datetime: first occurring week of day since day
        """
        # get days ahead of target
        days_ahead = weekday - 1 - day.weekday()
        # target day already happened this week if not today
        if days_ahead <= -1:
            # next week
            days_ahead += 7
        # get the date for the specific day of week
        return day + timedelta(days=days_ahead)
