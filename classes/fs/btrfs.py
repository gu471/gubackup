""" Module for backing up via btrfs subvolume snapshot """

import logging
from subprocess import Popen, PIPE

LOG = logging.getLogger("BTR ")

class Btrfs(object):
    """ Backup via cp """

    def __init__(self):
        """ Constructor for Sysop """

    def copy(self, source, destination):
        """ Creates Snapshot

        Args:
            source (str): source path
            destination (str): path for snapshot
        """
        snapshot = Popen(
            ["btrfs",
             "subvolume",
             "snapshot",
             source,
             destination],
            stdout=PIPE,
            stderr=PIPE)
        out, err = snapshot.communicate()

        return self._event_handler(out=out, err=err)

    def delete(self, path):
        """ Deletes snapshot

        Args:
            path (str): directory of snapshot to been deleted recursive
        """
        delete = Popen(
            ["btrfs",
             "subvolume",
             "delete",
             path],
            stdout=PIPE,
            stderr=PIPE)
        out, err = delete.communicate()

        return self._event_handler(out=out, err=err)

    @classmethod
    def _event_handler(cls, out, err):
        if len(err) is not 0:
            LOG.warn(err.decode().replace("\n", "|"))
            return False
        elif len(out) is not 0:
            LOG.info(out.decode().replace("\n", "|"))
            return True
