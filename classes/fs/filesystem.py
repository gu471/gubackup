""" Module for filesystem operations on backup up """

import logging
from os import listdir
from os.path import isdir

from classes.fs.sysop import Sysop
from classes.fs.btrfs import Btrfs

LOG = logging.getLogger("FS  ")

class FileSystem(object):
    """ Backup via cp """

    def __init__(self, config):
        """ Constructor for Sysop """

        # get and prepare config
        self.config = config
        self.source = config.source
        self.destination = config.destination
        self.type = config.backup_operation_type

    def do_backup(self, current_day, holding_days):
        """ Does backup with given data

        Args:
            current_day (str): the current day, where the backup is created
            holding_days ([str, ]): a list of days for holding the specific backup
                                    current day has to be included
        """

        backup_folder = self.destination + current_day

        # copy source to destination
        self._backup_create(self.source, backup_folder)

        # clear destination directory from not needed backups
        self.delete_unneeded(holding_days=holding_days)

    def delete_unneeded(self, holding_days):
        """ Checks if not held backups exists

        Args:
            holding_days ([str,]): a list of days for holding the specific backup
        """
        # get list of elements in destination dir
        destination_dir_list = listdir(self.destination)

        # for each item in destination directory
        for directory_item in destination_dir_list:
            # if item is directory
            if isdir(self.destination + directory_item):
                # if directory is not in list holding_days
                if directory_item not in holding_days:
                    # delete item
                    to_delete = self.destination + directory_item
                    self._backup_delete(to_delete)

    def _backup_delete(self, path):
        """ Deletes not needed backups

        Args:
            path (str): path of not needed backup
        """
        if self.type is self.config.SYSOP:
            if Sysop().delete(path=path) is True:
                LOG.info("deleting " + path + " as not held anymore")
            else:
                LOG.warn("backup " + path + " not deleted")
        elif self.type is self.config.BTRFS:
            if Btrfs().delete(path=path) is True:
                LOG.info("deleting " + path + " as not held anymore")
            else:
                LOG.warn("backup " + path + " not deleted")

    def _backup_create(self, source, backup_folder):
        """ Creates backup

        Args:
            source (str): source path of backup
            backup_folder (str): destination to backup from
        """
        if self.type is self.config.SYSOP:
            if Sysop().copy(source=source, destination=backup_folder) is True:
                LOG.info("created " + backup_folder + " as full backup")
            else:
                LOG.warn("backup " + backup_folder + " not created")
        elif self.type is self.config.BTRFS:
            if Btrfs().copy(source=source, destination=backup_folder) is True:
                LOG.info("created " + backup_folder + " as full backup")
            else:
                LOG.warn("backup " + backup_folder + " not created")
