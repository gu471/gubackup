""" Module for "backing" up via cp, not recommended for production """

import logging
import sys
from shutil import copytree, rmtree

LOG = logging.getLogger("SYS ")

class Sysop(object):
    """ Backup via cp """

    def __init__(self):
        """ Constructor for Sysop """

    @classmethod
    def copy(cls, source, destination):
        """ Copies directory recursive

        Args:
            source (str): path to copy from
            destination (str): path to copy to
        """
        try:
            copytree(source, destination)
            return True
        # pylint: disable=undefined-variable
        except FileExistsError:
            LOG.warn("cannot create " + destination + ", already exists")
            return False

    @classmethod
    def delete(cls, path):
        """ Deletes directory recursive

        Args:
            path (str): directory to been deleted recursive
        """
        try:
            rmtree(path)
            return True
        # pylint: disable=bare-except
        except:
            LOG.warn(sys.exc_info()[0])
            return False
