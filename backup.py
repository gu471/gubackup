""" Module for starter """

import logging

from config import Config
from classes.logic.backupday import BackupDay
from classes.fs.filesystem import FileSystem

LOG = logging.getLogger("MAIN")

now = BackupDay(config=Config())
paths = []
for x in now.get_backup_list():
    paths.append(x.get_directory_string_now())
dir_now = now.get_directory_string_now()
if not dir_now.__contains__("none"):
    FileSystem(config=Config()).do_backup(current_day=dir_now, holding_days=paths)
